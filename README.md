# Python Wrapper for OCRmyPDF to process PDF Documents From Filemaker

This software is designed to fetch PDFs from a Filemaker Server data API and OCR them and upload them back to the Filemaker Database.

## Requires
* Python3
* OCRmyPDF
* Pikepdf
* Ghostscript (gs)
* Filemaker data API user and enabled within Filemaker Server
* Tested with Filemaker 18+, but as long as the Filemaker API Supports V1, it would likely work.

## Usage
* Ensure dependancies are met
* Ensure your Filemaker database has a layout with a field PDF container field AND a text field that stores a simple "True/False".
* The True/False field should be scripted within the database to change to 'True' if the PDF container has changed. It will be set to 'False' after processing.
* Edit the fmConf.py file for appropriate settings
* Run with 'python3 procPDFs.py' 
* Optionally add to cron daemon to run regularly OR have a Filemaker script trigger it.

