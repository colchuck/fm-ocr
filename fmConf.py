#!/bin/python3

#filemaker user with API read/write privileges
fmAuth = b'apiuser:password'

#host to connect to, assumes regular installation directory AND HTTPS
fmHost = "yourHost.com"

#name of the filemaker database
fmDB = "yourDB"

#the layout should contain a field that says if the pdf has been processed yet (true/false, can be hidden), and the container that holds the PDF.
fmLayout = "yourLayout"

# these are the fields that you want to interact with within the layout
# in the pattern of [["field that says if processed or not (true/false)","PDF field","Name to add to PDF"]]
DBfields = [["processPDFField","PDFField","My Wonderful PDF"]]
