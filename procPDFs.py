#!/bin/python3
import base64
import json
import re
import requests
import ocrmypdf
import os
from shutil import which
from pikepdf import Pdf
from os import path
import atexit

#import the settings file for this project
from fmConf import fmAuth, fmHost, fmDB, DBfields, fmLayout

#check for lock file, if it doesn't exist, create, if it does, leave alone and exit
if not path.exists("/tmp/procPDFs.lck"):
    with open('/tmp/procPDFs.lck','w'):
        pass
else:
    exit()

#register to remove lock ONLY if it doesn't exit
def removeLock():
    os.remove("/tmp/procPDFs.lck")
    #cleanup working files as well
    try:
        if os.path.exists("procPDFs/toProc.pdf"):
            os.remove("procPDFs/toProc.pdf")
        if os.path.exists("procPDFs/toProc_nb.pdf"):
            os.remove("procPDFs/toProc_nb.pdf")
        if os.path.exists("procPDFs/fromProc.pdf"):
            os.remove("procPDFs/fromProc.pdf")
    except:
        print("Issue deleting old files, exiting")
        exit()

atexit.register(removeLock)

if not path.exists('procPDFs'):
    try:
        os.mkdir('procPDFs')
    except:
        print("Cannot create procPDFs directory")
        exit()


if which("gs") is None:
    print("Ghostscript not found, exiting.")
    exit()

#authenticate with filemaker
auth = base64.b64encode(fmAuth)
hdata = {"Authorization": "Basic " + auth.decode("utf-8"), "Content-Type": "application/json", "Content-Length": "0"}
req = requests.post("https://" + fmHost + "/fmi/data/v1/databases/"+fmDB+"/sessions", headers=hdata)
try:
    jdata = req.json()
except Exception as e:
    print("Cannot contact Filemaker database.")
    exit()
try:
    print(jdata["response"]["token"])
except Exception as e:
    print("Invalid Filemaker login.")
    exit()


for t in DBfields:

    #build query and authoriation headers
    qj = {"query": [{ t[0] : "True"}],"limit": "50"}
    hdata = {"Authorization": "Bearer " + jdata["response"]["token"], "Content-Type": "application/json"}

    #request first set of records and then decode them as json
    req = requests.post("https://" + fmHost + "/fmi/data/v1/databases/" + fmDB + "/layouts/" + fmLayout + "/_find", headers=hdata, json=qj, verify=True)
    try:
        dRecords = req.json()
    except Exception as e:
        dict["failMsg"] = "Cannot communicate with Filemaker API: " + str(e)
        exit()

    #display and check records
    print(dRecords)
    try:
        if dRecords["messages"][0]["message"] == "No records match the request":
            continue
    except:
        print("Issue reading records - check filemaker settings")
        exit()

    #loop through returned filemaker records
    for probs in dRecords["response"]["data"]:

        #clean up old data to reduce chance of erronious upload
        try:
            if os.path.exists("procPDFs/toProc.pdf"):
                os.remove("procPDFs/toProc.pdf")
            if os.path.exists("procPDFs/toProc_nb.pdf"):
                os.remove("procPDFs/toProc_nb.pdf")
            if os.path.exists("procPDFs/fromProc.pdf"):
                os.remove("procPDFs/fromProc.pdf")
        except:
            print("Issue deleting old files, exiting")
            exit()


        #make sure record's pdf is valid
        if (probs["fieldData"][t[1]].strip() != ""):
            try:
                res = requests.get(probs["fieldData"][t[1]])
                with open("procPDFs/toProc.pdf",'wb') as f:
                    f.write(res.content)
            except Exception as e:
                print("Error fetching PDF: " + str(e))
                exit()
        else:
            #we have a blank PDF field and a msg to process it - reset the messages
            qj = {"fieldData": {t[0]: "False"}}
            hdata = {"Authorization": "Bearer " + jdata["response"]["token"], "Content-Type": "application/json"}
            req = requests.patch("https://" + fmHost + "/fmi/data/v1/databases/" + fmDB + "/layouts/" + fmLayout + "/records/" + probs["recordId"], headers=hdata, json=qj)
            continue

        #load pdf into pikepdf and create new blank pdf
        try:
            dpdf = Pdf.open('procPDFs/toProc.pdf')

        except Pdf._qpdf.PdfError as e:
            #doesn't appear to be a pdf, reset messsages and skip
            qj = {"fieldData": {t[0]: "False"}}
            hdata = {"Authorization": "Bearer " + jdata["response"]["token"], "Content-Type": "application/json"}
            req = requests.patch("https://" + fmHost + "/fmi/data/v1/databases/" + fmDB + "/layouts/" + fmLayout + "/records/" + probs["recordId"], headers=hdata, json=qj)
            continue

        except Exception as e:
            #we can't read the pdf for some reason... maybe not a pdf?
            print("Error: " + str(e))
            exit()

        opdf = Pdf.new()
        ct = 1

        #iterate over pages and send them to ghostscript to check output for blank pages
        for p in dpdf.pages:

            #I know python-ghostscript exists - but I couldn't manage to get it work and it seemed to just be a glorified frontend for the exact same thing
            n = os.popen("gs -o -  -dFirstPage=" + str(ct) + " -dLastPage=" + str(ct) + " -sDEVICE=inkcov procPDFs/toProc.pdf | grep CMYK ")
            reson = n.read()
            reson = reson.split()
            try:
                perc = float(reson[0]) + float(reson[1]) + float(reson[2]) + float(reson[3])
                print("Page #"+str(ct) + " -- %.4f" % perc)
            except:
                print("Problem with Ghostscript or Ghostscript output")
                exit()
            if perc > .001:
                opdf.pages.append(dpdf.pages[ct-1])
            else:
                print("Removing page.")
            ct += 1

        #output new pdf
        opdf.save("procPDFs/toProc_nb.pdf")

        #if blank page detection created no file, exit b/c something is wrong
        if not os.path.exists("procPDFs/toProc_nb.pdf"):
            print("Error with blank page detection.")
            exit()

        #attempt OCR on PDF
        skippdf = False
        try:
            ocrmypdf.ocr("procPDFs/toProc_nb.pdf","procPDFs/fromProc.pdf",deskew=True, clean=True, force_ocr=True, rotate_pages=True ,rotate_pages_threshold=5.5,language="eng",title=t[2])

        #if it already has OCR data, just skip it
        except ocrmypdf.exceptions.PriorOcrFoundError as e:
            print( "Document already has ocr.")
            skippdf = True

        #if we generally run into OCR errors - skip it and move on
        except Exception as e:
            print("Error " + str(e))
            skippdf = True

        #upload PDF back to filemaker
        if not skippdf:
            phdata = {"Authorization": "Bearer " + jdata["response"]["token"]}
            up = {"upload": open('procPDFs/fromProc.pdf','rb')}
            reqa = requests.post("https://" + fmHost + "/fmi/data/v1/databases/" + fmDB + "/layouts/" + fmLayout + "/records/" + probs["recordId"] + "/containers/" + t[1], headers=phdata, verify=True, files=up)
            print(reqa.content)

        #set to false because we checked either way
        qj = {"fieldData": {t[0]: "False"}}
        hdata = {"Authorization": "Bearer " + jdata["response"]["token"], "Content-Type": "application/json"}
        req = requests.patch("https://" + fmHost + "/fmi/data/v1/databases/" + fmDB + "/layouts/" + fmLayout + "/records/" + probs["recordId"], headers=hdata, json=qj)

exit()
